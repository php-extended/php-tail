<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-tail library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Tail\OutputBufferException;
use PHPUnit\Framework\TestCase;

/**
 * OutputBufferExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Tail\OutputBufferException
 *
 * @internal
 *
 * @small
 */
class OutputBufferExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var OutputBufferException
	 */
	protected OutputBufferException $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString(OutputBufferException::class, $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new OutputBufferException(__FILE__, 0);
	}
	
}
