<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-tail library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Tail\FileNotFoundException;
use PHPUnit\Framework\TestCase;

/**
 * FileNotFoundExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Tail\FileNotFoundException
 *
 * @internal
 *
 * @small
 */
class FileNotFoundExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var FileNotFoundException
	 */
	protected FileNotFoundException $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString(FileNotFoundException::class, $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new FileNotFoundException(__FILE__, 0);
	}
	
}
