<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-tail library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Tail\IllegalArgumentException;
use PHPUnit\Framework\TestCase;

/**
 * IllegalArgumentExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Tail\IllegalArgumentException
 *
 * @internal
 *
 * @small
 */
class IllegalArgumentExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var IllegalArgumentException
	 */
	protected IllegalArgumentException $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString(IllegalArgumentException::class, $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new IllegalArgumentException(__FILE__, 0);
	}
	
}
