<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-tail library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Tail\IOException;
use PHPUnit\Framework\TestCase;

/**
 * IOExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Tail\IOException
 *
 * @internal
 *
 * @small
 */
class IOExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var IOException
	 */
	protected IOException $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString(IOException::class, $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new IOException(__FILE__, 0);
	}
	
}
