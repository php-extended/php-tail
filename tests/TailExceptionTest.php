<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-tail library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Tail\TailException;
use PHPUnit\Framework\TestCase;

/**
 * TailExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Tail\TailException
 *
 * @internal
 *
 * @small
 */
class TailExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var TailException
	 */
	protected TailException $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString(TailException::class, $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new TailException(__FILE__, 0);
	}
	
}
