<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-tail library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Tail\FileTooBigException;
use PHPUnit\Framework\TestCase;

/**
 * FileTooBigExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Tail\FileTooBigException
 *
 * @internal
 *
 * @small
 */
class FileTooBigExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var FileTooBigException
	 */
	protected FileTooBigException $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString(FileTooBigException::class, $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new FileTooBigException(__FILE__, 0);
	}
	
}
