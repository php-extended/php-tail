<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-tail library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Tail\TailShellException;
use PHPUnit\Framework\TestCase;

/**
 * TailShellExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Tail\TailShellException
 *
 * @internal
 *
 * @small
 */
class TailShellExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var TailShellException
	 */
	protected TailShellException $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString(TailShellException::class, $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new TailShellException(__FILE__, 0);
	}
	
}
