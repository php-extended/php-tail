<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-tail library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Tail;

/**
 * FileTooBigExcepion class file.
 *
 * This exception represents a try to read on a file that is larger than the
 * int max capacity on the system. This exception may occur if the file length
 * exceeds the INT32 capacity on windows 32 bits systems. It should not occur
 * on 64 bits php (except if the size is larger than 2^63 bytes)
 *
 * @author Anastaszor
 */
class FileTooBigException extends TailException
{
	
	/**
	 * Builds a new FileTooBigException object.
	 *
	 * @param string $filename the name of targeted file
	 * @param integer $nblines the number of lines that were demanded
	 * @param ?integer $hint an estimation of the line length in that file
	 */
	public function __construct(string $filename, int $nblines, ?int $hint = null)
	{
		parent::__construct(
			$filename,
			$nblines,
			$hint,
			\strtr('File {filename} is too big to be read.', ['{filename}' => $filename]),
			500,
		);
	}
	
}
