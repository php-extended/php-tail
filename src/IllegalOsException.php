<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-tail library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Tail;

/**
 * IllegalOsException class file.
 *
 * This exception is thrown when the cheating method to tail a file, i.e.
 * calling the tail unix function is choosen, but the running operating system
 * is not supporting that function.
 *
 * @author Anastaszor
 */
class IllegalOsException extends TailException
{
	
	/**
	 * Builds a new IllegalOsException object.
	 *
	 * @param string $filename the name of targeted file
	 * @param integer $nblines the number of lines that were demanded
	 * @param ?integer $hint an estimation of the line length in that file
	 */
	public function __construct(string $filename, int $nblines, ?int $hint = null)
	{
		parent::__construct(
			$filename,
			$nblines,
			$hint,
			'You can\'t try to call unix\'s tail function on a non unix system.',
			500,
		);
	}
	
}
