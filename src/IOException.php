<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-tail library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Tail;

/**
 * IOException class file.
 *
 * This exception represents a read error on targeted file that occured during
 * the reading process of tail.
 *
 * @author Anastaszor
 */
class IOException extends TailException
{
	
	/**
	 * Builds a new IOException object.
	 *
	 * @param string $filename the name of targeted file
	 * @param integer $nblines the number of lines that were demanded
	 * @param ?integer $hint an estimation of the line length in that file
	 */
	public function __construct(string $filename, int $nblines, ?int $hint = null)
	{
		parent::__construct(
			$filename,
			$nblines,
			$hint,
			\strtr('Error in reading file {filename}', ['{filename}' => $filename]),
			500,
		);
	}
	
}
