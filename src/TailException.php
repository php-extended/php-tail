<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-tail library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Tail;

use Exception;

/**
 * TailException class file.
 *
 * This is a generic purpose exception for all of that may occur when tailing
 * a file with this library.
 *
 * @author Anastaszor
 */
class TailException extends Exception
{
	
	/**
	 * The path of the file that was given.
	 *
	 * @var string the path of the file
	 */
	protected string $_filename;
	
	/**
	 * The number of lines that were asked.
	 *
	 * @var int the number of lines
	 */
	protected int $_nblines;
	
	/**
	 * An estimation of the number of characters per line.
	 *
	 * @var ?int the median number of characters per line
	 */
	protected ?int $_hint = null;
	
	/**
	 * Builds a new TailException object.
	 *
	 * @param string $filename the name of the file
	 * @param integer $nblines the number of lines wanted
	 * @param ?integer $hint an estimation of the median number of characters per line
	 * @param ?string $message the message of the exception
	 * @param ?integer $code the error code associated to this exception
	 */
	public function __construct(string $filename, int $nblines, ?int $hint = null, ?string $message = null, ?int $code = null)
	{
		parent::__construct((string) $message, (int) $code);
		$this->_filename = $filename;
		$this->_nblines = $nblines;
		$this->_hint = $hint;
	}
	
	/**
	 * Gets the file path that was asked.
	 *
	 * @return string
	 */
	public function getFilename() : string
	{
		return $this->_filename;
	}
	
	/**
	 * Gets the number of lines that was asked.
	 *
	 * @return int
	 */
	public function getNblines() : int
	{
		return $this->_nblines;
	}
	
	/**
	 * Gets the median number of characters per line that was given, or
	 * calculated, if applicable.
	 *
	 * @return ?int
	 */
	public function getHint() : ?int
	{
		return $this->_hint;
	}
	
}
