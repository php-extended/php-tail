<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-tail library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Tail;

use PhpExtended\System\OperatingSystem;
use Stringable;

/**
 * Tail class file.
 *
 * This class provides methods that tails given file and return an array with
 * the wanted final lines of that file.
 * This class is the equivalent of a `tail -n X filepath` unix command.
 *
 * @author Anastaszor
 * @see http://stackoverflow.com/questions/15025875/what-is-the-best-way-in-php-to-read-last-lines-from-a-file/15025877
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
class Tail implements Stringable
{
	
	/**
	 * If true, the given file should exists. This does not guarantee that
	 * that file will remain existant for as long as the tailing process will
	 * need it to be.
	 *
	 * @var boolean whether the given file exists
	 */
	protected bool $_fileExists = false;
	
	/**
	 * The path to the file. If it is a relative path, then all the process
	 * for finding files with fopen() will be used, and this library does not
	 * modify any of that configuration.
	 *
	 * @var string the path of targeted file
	 */
	protected string $_filePath;
	
	/**
	 * Builds a new Tail object on targeted file path. If targeted file path
	 * does not exists, the object will still be correcly constructed. Check
	 * the <code>getFileExists()</code> method to be safe as tailing targeted
	 * file.
	 *
	 * @param string $filepath the targeted file path, may be absolute or
	 *                         relative, all rules for finding it with fopen will be unchanged
	 */
	public function __construct(string $filepath)
	{
		$this->_filePath = $filepath;
		$this->_fileExists = \is_file($this->_filePath);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the file path that was given to this object.
	 *
	 * @return string the file path of the target
	 */
	public function getFilePath() : string
	{
		return $this->_filePath;
	}
	
	/**
	 * Gets whether target file exists or not.
	 *
	 * @return boolean
	 */
	public function fileExists() : bool
	{
		return $this->_fileExists;
	}
	
	/**
	 * Decides the best way to tail the given file given the environment.
	 *
	 * @param integer $nblines the number of lines wanted
	 * @param ?integer $hint a median number of bytes for each line in target file
	 * @param boolean $silent if false, any error will throw an exception. If
	 *                        true, any error will silently return an empty array.
	 * @return array<integer, string> the wanted tailed lines
	 * @throws FileNotFoundException
	 * @throws FileTooBigException
	 * @throws IllegalArgumentException
	 * @throws IllegalOsException
	 * @throws IOException
	 * @throws OutputBufferException
	 * @throws TailShellException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function smart(int $nblines, ?int $hint = null, bool $silent = false) : array
	{
		if(!$this->_fileExists)
		{
			if($silent)
			{
				return [];
			}
			
			throw new FileNotFoundException($this->_filePath, $nblines, $hint);
		}
		
		$nblines = (int) $nblines;
		if(0 >= $nblines)
		{
			if($silent)
			{
				return [];
			}
			
			throw new IllegalArgumentException($this->_filePath, $nblines, $hint);
		}
		
		$filelength = \filesize($this->_filePath);
		if(false === $filelength)
		{
			if($silent)
			{
				return [];
			}
			
			throw new IOException($this->_filePath, $nblines, $hint);
		}
		
		if(0 === $filelength)
		{
			return [];
		}
		
		if($this->isUnixSystem())
		{
			return $this->cheat($nblines, $hint, $silent);
		}
			
		if(2 >= $nblines)
		{
			return $this->single($nblines, $hint, $silent);
		}
		
		if(10240 >= $filelength)	// 10kB
		{
			return $this->naive($nblines, $hint, $silent);
		}
		
		return $this->dynamic($nblines, $hint, $silent);
	}
	
	/**
	 * The naive approach. This will load the whole file into php's buffer
	 * and extract only the last wanted lines.
	 *
	 * @param int $nblines the number of lines wanted
	 * @param int $hint a median number of bytes for each line in target file
	 * @param boolean $silent if false, any error will throw an exception. If
	 *                        true, any error will silently return an empty array.
	 * @return array<integer, string> the wanted tailed lines
	 * @throws FileNotFoundException
	 * @throws IllegalArgumentException
	 * @throws IOException
	 */
	public function naive(int $nblines, ?int $hint = null, bool $silent = false) : array
	{
		if(!$this->_fileExists)
		{
			if($silent)
			{
			return [];
			}
			
			throw new FileNotFoundException($this->_filePath, $nblines, $hint);
		}
		
		$nblines = (int) $nblines;
		if(0 >= $nblines)
		{
			if($silent)
			{
				return [];
			}
			
			throw new IllegalArgumentException($this->_filePath, $nblines, $hint);
		}
		
		$filedata = \file($this->_filePath);
		if(false === $filedata)
		{
			if($silent)
			{
				return [];
			}
			
			throw new IOException($this->_filePath, $nblines, $hint);
		}
		
		return \array_slice($filedata, -$nblines);
	}
	
	/**
	 * The cheating approach. This will call the tail unix function and get
	 * the return from it.
	 *
	 * @param int $nblines the number of lines wanted
	 * @param int $hint a median number of bytes for each line in target file
	 * @param boolean $silent if false, any error will throw an exception. If
	 *                        true, any error will silently return an empty array.
	 * @return array<integer, string> the wanted tailed lines
	 * @throws FileNotFoundException
	 * @throws IllegalArgumentException
	 * @throws IllegalOsException
	 * @throws IOException
	 * @throws OutputBufferException
	 * @throws TailShellException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function cheat(int $nblines, ?int $hint = null, bool $silent = false) : array
	{
		if(!$this->_fileExists)
		{
			if($silent)
			{
				return [];
			}
			
			throw new FileNotFoundException($this->_filePath, $nblines, $hint);
		}
		
		$nblines = (int) $nblines;
		if(0 >= $nblines)
		{
			if($silent)
			{
				return [];
			}
			
			throw new IllegalArgumentException($this->_filePath, $nblines, $hint);
		}
		
		if(!$this->isUnixSystem())
		{
			if($silent)
			{
				return [];
			}
			
			throw new IllegalOsException($this->_filePath, $nblines, $hint);
		}
		
		if(!\function_exists('ob_start'))
		{
			if($silent)
			{
				return [];
			}
			
			throw new OutputBufferException($this->_filePath, $nblines, $hint);
		}
		
		if(!\ob_start())
		{
			if($silent)
			{
				return [];
			}
			
			throw new OutputBufferException($this->_filePath, $nblines, $hint);
		}
		
		$returnvar = 0;
		\passthru('tail -n '.((string) $nblines).' '.\escapeshellarg($this->_filePath), $returnvar);
		if(0 !== $returnvar)
		{
			\ob_end_clean();
			if($silent)
			{
				return [];
			}
			
			throw new TailShellException($this->_filePath, $nblines, $hint);
		}
		
		$results = \ob_get_clean();
		if(false === $results)
		{
			if($silent)
			{
				return [];
			}
			
			throw new OutputBufferException($this->_filePath, $nblines, $hint);
		}
		
		return \array_map('trim', \explode("\n", $results));
	}
	
	/**
	 * The single byte buffer approach. This will read backward from the end
	 * of the file each char until wanted lines are read.
	 *
	 * @param int $nblines the number of lines wanted
	 * @param int $hint a median number of bytes for each line in target file
	 * @param boolean $silent if false, any error will throw an exception. If
	 *                        true, any error will silently return an empty array.
	 * @return array<integer, string> the wanted tailed lines
	 * @throws FileNotFoundException
	 * @throws IllegalArgumentException
	 * @throws IOException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	public function single(int $nblines, ?int $hint = null, bool $silent = false) : array
	{
		if(!$this->_fileExists)
		{
			if($silent)
			{
				return [];
			}
			
			throw new FileNotFoundException($this->_filePath, $nblines, $hint);
		}
		
		$nblines = (int) $nblines;
		if(0 >= $nblines)
		{
			if($silent)
			{
				return [];
			}
			
			throw new IllegalArgumentException($this->_filePath, $nblines, $hint);
		}
		
		$handle = \fopen($this->_filePath, 'r');
		if(false === $handle)
		{
			if($silent)
			{
				return [];
			}
			
			throw new IOException($this->_filePath, $nblines, $hint);
		}
		
		$linecounter = $nblines;
		$pos = -2;
		$beginning = false;
		$text = [];
		
		while(0 < $linecounter)
		{
			$char = ' ';
			
			while("\n" !== $char)
			{
				$seek = \fseek($handle, $pos, \SEEK_END);
				if(-1 === $seek)
				{
					$beginning = true;
					break;
				}
				
				$char = \fgetc($handle);
				if(false === $char)
				{
					if($silent)
					{
						return [];
					}
					
					throw new IOException($this->_filePath, $nblines, $hint);
				}
				
				$pos--;
			}
			
			$linecounter--;
			if($beginning)
			{
				$rewind = \rewind($handle);
				if(false === $rewind)
				{
					if($silent)
					{
						return [];
					}
					
					throw new IOException($this->_filePath, $nblines, $hint);
				}
			}
			$data = \fgetc($handle);
			if(false === $data)
			{
				if($silent)
				{
					return [];
				}
				
				throw new IOException($this->_filePath, $nblines, $hint);
			}
			
			$text[$nblines - $linecounter - 1] = $data;
			if($beginning)
			{
				break;
			}
		}
		
		\fclose($handle);
		
		return \array_reverse($text);
	}
	
	/**
	 * The simple buffer approach. This will read backward from the end of the
	 * file with a fixed size buffer until wanted lines are read.
	 *
	 * @param int $nblines the number of lines wanted
	 * @param int $hint a median number of bytes for each line in target file
	 * @param boolean $silent if false, any error will throw an exception. If
	 *                        true, any error will silently return an empty array.
	 * @return array<integer, string> the wanted tailed lines
	 * @throws FileNotFoundException
	 * @throws IllegalArgumentException
	 * @throws IOException
	 */
	public function simple(int $nblines, ?int $hint = null, bool $silent = false) : array
	{
		if(!$this->_fileExists)
		{
			if($silent)
			{
				return [];
			}
			
			throw new FileNotFoundException($this->_filePath, $nblines, $hint);
		}
		
		$nblines = (int) $nblines;
		if(0 >= $nblines)
		{
			if($silent)
			{
				return [];
			}
			
			throw new IllegalArgumentException($this->_filePath, $nblines, $hint);
		}
		
		if(null === $hint)
		{
			// just assuming that this is text file with 40 char per line as a
			// median basis. May be larger if it is a log file
			$hint = 40;
			if(\mb_strrpos($this->_filePath, '.log') === (int) \mb_strlen($this->_filePath) - 4)
			{
				$hint = 240;
			}
		}
		
		$buffer = 4096;
		
		return $this->find($nblines, $buffer, $hint, $silent);
	}
	
	/**
	 * The dynamic buffer approach. This will read backward from the end of the
	 * file with a fixed size buffer which is proportional to the size of the
	 * wanted chunk of that file.
	 *
	 * @param int $nblines the number of lines wanted
	 * @param int $hint a median number of bytes for each line in target file
	 * @param boolean $silent if false, any error will throw an exception. If
	 *                        true, any error will silently return an empty array.
	 * @return array<integer, string> the wanted tailed lines
	 * @throws FileNotFoundException
	 * @throws IllegalArgumentException
	 * @throws IOException
	 */
	public function dynamic(int $nblines, ?int $hint = null, bool $silent = false) : array
	{
		if(!$this->_fileExists)
		{
			if($silent)
			{
				return [];
			}
			
			throw new FileNotFoundException($this->_filePath, $nblines, $hint);
		}
		
		$nblines = (int) $nblines;
		if(0 >= $nblines)
		{
			if($silent)
			{
				return [];
			}
			
			throw new IllegalArgumentException($this->_filePath, $nblines, $hint);
		}
		
		if(null === $hint)
		{
			// just assuming that this is text file with 40 char per line as a
			// median basis. May be larger if it is a log file
			$hint = 40;
			if(\mb_strrpos($this->_filePath, '.log') === (int) \mb_strlen($this->_filePath) - 4)
			{
				$hint = 240;
			}
		}
		
		$buffer = (int) ($hint * $nblines / 10);
		
		return $this->find($nblines, $buffer, $hint, $silent);
	}
	
	/**
	 * Gets whether this running system is from an unix family. This is called
	 * only assuming that ALL linux based operating system have the tail -n
	 * command available and in their $PATH, so it can direcly be called by
	 * php's passthru() function.
	 *
	 * @return boolean true if `tail -n` is supported
	 */
	protected function isUnixSystem() : bool
	{
		return OperatingSystem::get()->isUnix();
	}
	
	/**
	 * Extracts the wanted number of lines according to the given buffer
	 * length.
	 *
	 * @param integer $nblines the number of lines wanted
	 * @param integer $buffer the size of the buffer that will be used to seek
	 * @param integer $hint an estimation of the median line length on this file
	 * @param boolean $silent if false, any error will throw an exception.
	 *                        If true, any error will silently return an empty array.
	 * @return array<integer, string> the wanted tailed lines
	 * @throws FileNotFoundException
	 * @throws IllegalArgumentException
	 * @throws IOException
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	protected function find(int $nblines, int $buffer, int $hint, bool $silent) : array
	{
		$lines = $nblines;
		$file = \fopen($this->_filePath, 'r');
		if(false === $file)
		{
			if($silent)
			{
				return [];
			}
			
			throw new IOException($this->_filePath, $nblines, $hint);
		}
		
		$seek = \fseek($file, -1, \SEEK_END);
		if(-1 === $seek)
		{
			\fclose($file);
			if($silent)
			{
				return [];
			}
			
			throw new IOException($this->_filePath, $nblines, $hint);
		}
		
		$read = \fread($file, 1);
		if(false === $read)
		{
			\fclose($file);
			if($silent)
			{
				return [];
			}
			
			throw new IOException($this->_filePath, $nblines, $hint);
		}
		
		if("\n" === $read)
		{
			$lines--;
		}
		
		// faster initialisation
		$telld = \ftell($file);
		if(false === $telld)
		{
			\fclose($file);
			if($silent)
			{
				return [];
			}
			
			throw new IOException($this->_filePath, $nblines, $hint);
		}
		$seek = \min($telld, $hint * $nblines);
		$seeked = \fseek($file, -$seek, \SEEK_CUR);
		if(-1 === $seeked)
		{
			\fclose($file);
			if($silent)
			{
				return [];
			}
			
			throw new IOException($this->_filePath, $nblines, $hint);
		}
		
		$chunk = \fread($file, \max(1, $seek));
		if(false === $chunk)
		{
			\fclose($file);
			if($silent)
			{
				return [];
			}
			
			throw new IOException($this->_filePath, $nblines, $hint);
		}
		
		$output = $chunk;
		$seeked = \fseek($file, -\mb_strlen($chunk, '8bit'), \SEEK_CUR);
		if(-1 === $seeked)
		{
			\fclose($file);
			if($silent)
			{
				return [];
			}
			
			throw new IOException($this->_filePath, $nblines, $hint);
		}
		$lines -= (int) \mb_substr_count($chunk, "\n");
		
		// while we want more
		while(0 < ((int) \ftell($file)) && 0 <= $lines)
		{
			// figure out how far back we should jump
			$telld = \ftell($file);
			if(false === $telld)
			{
				\fclose($file);
				if($silent)
				{
					return [];
				}
				
				throw new IOException($this->_filePath, $nblines, $hint);
			}
			
			$seek = \min($telld, $buffer);
			
			// do the jump backwards relative to where we are
			$seeked = \fseek($file, -$seek, \SEEK_CUR);
			if(-1 === $seeked)
			{
				\fclose($file);
				if($silent)
				{
					return [];
				}
				
				throw new IOException($this->_filePath, $nblines, $hint);
			}
			
			// read a chunk and prepend it to out output
			$chunk = \fread($file, \max(1, $seek));
			if(false === $chunk)
			{
				\fclose($file);
				if($silent)
				{
					return [];
				}
				
				throw new IOException($this->_filePath, $nblines, $hint);
			}
			$output = $chunk.$output;
			
			// jump back to where we started reading
			$seeked = \fseek($file, -\mb_strlen($chunk, '8bit'), \SEEK_CUR);
			if(-1 === $seeked)
			{
				\fclose($file);
				if($silent)
				{
					return [];
				}
				
				throw new IOException($this->_filePath, $nblines, $hint);
			}
			
			// decrease out line counter
			$lines -= (int) \mb_substr_count($chunk, "\n");
		}
		
		\fclose($file);
		
		// while we have too many lines
		// because the buffer size we might have read too many
		while(0 > $lines++)
		{
			$pos = \mb_strpos((string) $output, "\n");
			if(false === $pos)
			{
				break;
			}
			
			$output = (string) \mb_substr((string) $output, $pos + 1);
		}
		
		return \explode("\n", \str_replace(["\r\n", "\r"], ["\n", ''], \trim($output)));
	}
	
}
