# php-extended/php-tail
A smart way to tail files depending of the environment

![coverage](https://gitlab.com/php-extended/php-tail/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-tail/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-tail ^8`


## Usage

The basic usage of this library is as follows :

```php
use PhpExtended\Tail\Tail;
use PhpExtended\Tail\TailException;

$filename = "/../path/to/my/file.ext";
$tail = new Tail($filename);

try
{
	// 10 is the number of lines you want
	// 200 is the average number of chars on each line (optional)
	// false is to force throwing exceptions (optional, use true if you want silent mode)
	$lines = $tail->smart(10, 200, false);
}
catch(TailException $e)
{
	// do something is case it fails
}

```

This library proposes 6 methods to tail a file, which can be more or less 
performant depending on the context. They each follow the same signature 
(see sample code above).

Those methods are :
- `naive` : Loads the whole file into php, then retains only the last lines
- `cheat` : Uses underlying unix `tail -n` function
- `single` : Uses a signle byte buffer to read backwards the file
- `simple` : Uses a fixed size buffer to read backwards the file
- `dynamic` : Uses a dynamically sized buffer to read backwards the file
- `smart` : Tries to choose the best among those five to be the fastest (recommanded)

This library is inspired from this [specific stackoverflow topic](http://stackoverflow.com/questions/15025875/what-is-the-best-way-in-php-to-read-last-lines-from-a-file/15025877).


## License
MIT (See [license file](LICENSE)).
